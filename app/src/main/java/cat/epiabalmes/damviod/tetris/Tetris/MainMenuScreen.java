package cat.epiabalmes.damviod.tetris.Tetris;

import java.util.List;

import cat.epiabalmes.damviod.tetris.framework.Game;
import cat.epiabalmes.damviod.tetris.framework.Graphics;
import cat.epiabalmes.damviod.tetris.framework.Input.TouchEvent;
import cat.epiabalmes.damviod.tetris.framework.Screen;

public class MainMenuScreen extends Screen {
    public MainMenuScreen(Game game) {
        super(game);
    }


    public void update(float deltaTime) {

        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {

                if(inBounds(event, 64, 220, 219, 217) ) {
                    game.setScreen(new GameScreen(game));
                    Assets.gamesound.play();
                    Assets.click.play(1);
                    return;
                }


            }
        }
    }

    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.titulo, 10,0);

        g.drawPixmap(Assets.mainMenu, 64, 220);

    }

    public void pause() {
        Settings.save(game.getFileIO());
    }

    public void resume() {

    }

    public void dispose() {

    }
}


