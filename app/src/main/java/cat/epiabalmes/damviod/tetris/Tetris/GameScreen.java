package cat.epiabalmes.damviod.tetris.Tetris;

import android.media.MediaPlayer;
import android.view.MotionEvent;

import java.util.List;

import cat.epiabalmes.damviod.tetris.framework.Game;
import cat.epiabalmes.damviod.tetris.framework.Graphics;
import cat.epiabalmes.damviod.tetris.framework.Input.TouchEvent;
import cat.epiabalmes.damviod.tetris.framework.Screen;

public class GameScreen extends Screen {
    float tickTime = 0;
    float x;
    float y;
    float x1;
    float y1;
    boolean abajo = false;



    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver

    }



    GameState state = GameState.Running;
    Board board;


    public GameScreen(Game game) {


        super(game);
        board = new Board(game);


    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        tickTime++;

        if(state == GameState.Ready)
            updateReady(touchEvents);

        if(state == GameState.Running) {

            updateRunning(touchEvents, deltaTime);
            if (board.gameOver == true) {
                state = GameState.GameOver;
            }
            if (tickTime > 20) {
                board.update(deltaTime);
                if (abajo == true) {
                    if (board.end == true) {
                        abajo = false;
                    }
                    tickTime = 25;
                } else {
                    tickTime = 0;
                }


            }
        }
        if(state == GameState.Paused)
            updatePaused(touchEvents);

        if(state == GameState.GameOver)
            updateGameOver(touchEvents);


    }

    private void updateReady(List<TouchEvent> touchEvents) {
        if(touchEvents.size() > 0) state = GameState.Running;
    }

    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();

        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_DOWN) {

                x = event.x;
                y = event.y;
            }
            if (event.type == TouchEvent.TOUCH_UP) {

                if (event.x > 250 && event.y <60 ) {
                    Assets.click.play(1);
                    state = GameState.Paused;
                    return;


                }

                x1 = event.x;
                y1 = event.y;

                if (!abajo) {

                    if (y < y1) {
                        if (y - y1 < -70) {
                            abajo = true;
                        }
                    }

                    if (x < x1) {
                        if (x - x1 < -30) {

                            board.Mover_derecha();

                        }

                    } else if (x > x1) {
                        if (x - x1 > 30) {

                            board.Mover_izquierda();

                        }
                    } else {

                        board.NewArray();

                    }
                }

            }
        }

    }




    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    state = GameState.Ready;
                    return;

                }
                else if (event.x < 300 && event.y > 416) {
                    Assets.click.play(1);
                    state = GameState.Running;
                    return;

                }
            }
        }

    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    state = GameState.Ready;
                    return;

                }
                else if (event.x < 300 && event.y > 416) {
                    Assets.click.play(1);
                    game.setScreen(new GameScreen(game));
                    state = GameState.Running;
                    return;

                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background2, 0, 0);

        if(state == GameState.Ready)
            drawReadyUI();

        if(state == GameState.Running)
            drawRunningUI();

        board.render(deltaTime);
        if(state == GameState.Paused) {
            drawPausedUI();

        }
        if(state == GameState.GameOver) {
            drawGameOverUI();

        }


    }



    private void drawReadyUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 250, 0, 65, 128, 64, 64);


    }

    private void drawRunningUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 250, 0, 65, 128, 64, 64);



    }

    private void drawPausedUI() {



        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background3,0,0);

        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);

        g.drawPixmap(Assets.buttons, 250, 416, 0, 64, 64, 64);

    }

    private void drawGameOverUI() {

        Assets.gamesound.stop();
        Graphics g = game.getGraphics();


        g.drawPixmap(Assets.background2,0,0);

        g.drawPixmap(Assets.gameover,0,0);

        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);

        g.drawPixmap(Assets.buttons, 250, 416, 0, 64, 64, 64);

        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();







    }

    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }


            x += srcWidth;
        }
    }

    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
        Assets.gamesound.pause();

    }

    @Override
    public void resume() {
       Assets.gamesound.play();
    }

    @Override
    public void dispose() {

    }
}

