package cat.epiabalmes.damviod.tetris.framework;

public interface Audio {
    public Music newMusic(String filename);

    public Sound newSound(String filename);
}
