package cat.epiabalmes.damviod.tetris.Tetris;


import cat.epiabalmes.damviod.tetris.framework.Music;
import cat.epiabalmes.damviod.tetris.framework.Pixmap;
import cat.epiabalmes.damviod.tetris.framework.Sound;
/**
 * Created by damviod on 11/12/15.
 */
public class Assets {

    public static Pixmap background;
    public static Pixmap background2;
    public static Pixmap background3;
    public static Pixmap mainMenu;

    public static Pixmap buttons;

    public static Pixmap tile;
    public static Pixmap tile_azul;
    public static Pixmap tile_azul_flojo;
    public static Pixmap tile_amarillo;

    public static Pixmap tile_verde;
    public static Pixmap tile_lila;
    public static Pixmap titulo;

    public static Pixmap gameover;




    public static Sound click;
    public static Music gamesound;


}
