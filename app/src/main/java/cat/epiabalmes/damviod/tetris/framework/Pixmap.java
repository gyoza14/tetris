package cat.epiabalmes.damviod.tetris.framework;
import cat.epiabalmes.damviod.tetris.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
