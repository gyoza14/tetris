package cat.epiabalmes.damviod.tetris.Tetris;


import java.util.Random;

import cat.epiabalmes.damviod.tetris.framework.Game;
import cat.epiabalmes.damviod.tetris.framework.Graphics;


public class Board {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 16;



    Game game;
    Pieza piece;

    boolean lim_der = false;
    boolean lim_izq = false;
    public boolean gameOver = false;
    boolean linea = false;

    boolean end = false;

    boolean rotable = false;
    int fields[][] = new int[WORLD_WIDTH+5][WORLD_HEIGHT+5];
    boolean fet = false;


    public Board(Game game) {


        piece = new Pieza(2);


        this.game = game;


    }


    public void update(float deltaTime) {

        end = false;

        for (int c = 0; c < WORLD_HEIGHT; c++) {
            linea = true;
            for (int l = 0; l < WORLD_WIDTH; l++) {
                if (fields[l][c] == 0) {
                    linea = false;
                }


            }
            if (linea) {
                Borrar_Linea(c);

            }


        }


        if (piece.y + piece.h == WORLD_HEIGHT - 1) {

            for (int i = 0; i < piece.w; i++) {

                for (int j = 0; j < piece.h; j++) {

                    if (fields[piece.x + i][piece.y + j] != piece.forma[i][j]) {
                        fields[piece.x + i][piece.y + j] = piece.current;
                        end = true;
                    }

                }
            }
            Random rand = new Random();
            int n = rand.nextInt(5) + 1;
            piece = new Pieza(n);
        } else {

            for (int i = 0; i < piece.w; i++) {

                for (int j = 0; j < piece.h; j++) {

                    if (piece.y >= 0) {

                        if (fields[piece.x + i][piece.y + j + 1] != 0) {
                            if (fields[piece.x + i][piece.y + j] != piece.forma[i][j]) {
                                if (piece.y == 0) {
                                    gameOver = true;

                                }
                                fet = true;
                            }

                        }

                    }


                }
            }

            if (fet == false) {
                piece.y++;
            } else {
                for (int a = 0; a < piece.w; a++) {

                    for (int b = 0; b < piece.h; b++) {

                        if (fields[piece.x + a][piece.y + b] != piece.forma[a][b]) {
                            if (fields[piece.x + a][piece.y + b] == 0) {

                                fields[piece.x + a][piece.y + b] = piece.current;

                                end = true;
                            }
                        }

                    }
                }
                Random rand = new Random();
                int n = rand.nextInt(6) + 1;
                piece = new Pieza(n);
                fet = false;
            }

        }

    }


    public void Borrar_Linea(int linea_a_borrar) {

        for (int a = 0; a < WORLD_WIDTH; a++) {
            fields[a][linea_a_borrar] = 0;

            if (a == 9) {

                Baixar_linea(linea_a_borrar);

            }
        }


    }

    public int[][] Rotate() {


        final int M = piece.forma.length;
        final int N = piece.forma[0].length;

        int[][] ret = new int[N][M];

        for (int r = 0; r < M; r++) {
            for (int c = 0; c < N; c++) {


                ret[c][M - 1 - r] = piece.forma[r][c];

            }

        }

        return  ret;


    }



    public boolean CanRotate()
    {

        rotable = false;
        int[][] ret = Rotate();

        int w = piece.w;
        int h = piece.h;


        if (piece.x + piece.h <= WORLD_WIDTH) {
            if (piece.y + piece.w < WORLD_HEIGHT) {

                rotable = true;
            }
        }

        if (piece.y >=0) {
            for (int j = 0; j < piece.h; j++) {

                for (int l = 0; l < piece.w; l++)

                    if (fields[piece.x + j][piece.y + l] != 0) {

                        rotable = false;
                    }
            }

        }

        return rotable;
    }


    public  void NewArray()
    {
        int[][] newArry = Rotate();

        if (CanRotate()) {
            final int M = piece.forma.length;
            final int N = piece.forma[0].length;

            piece.forma = new int[N][M];

            for (int i = 0; i < piece.forma.length; i++) {
                for (int j = 0; j < piece.forma[i].length; j++) {
                    piece.forma[i][j] = newArry[i][j];
                }
            }

            int w = piece.w;
            int h = piece.h;

            piece.w = h;
            piece.h = w;


        }

    }

    public  void Baixar_linea(int linia_baixar)
    {

        for (int i = 0; i <WORLD_WIDTH; i ++) {

            for (int j = WORLD_HEIGHT-1; j> 0; j--) {

                if (j < linia_baixar +1) {
                    fields[i][j] = fields[i][j-1];

                }



            }

        }
    }
    public void Mover_derecha()
    {

        if (piece.x + piece.w != WORLD_WIDTH ) {

            lim_der = false;

            for (int j = 0; j < piece.h; j++) {
                if (piece.y >= 0) {
                    if (fields[piece.x + piece.w][piece.y + j] != 0) {

                        lim_der = true;
                    }


                }
            }

            if (!lim_der)
            {
                piece.x++;
            }

        }

    }
    public void Mover_izquierda() {
        if (piece.x != 0) {

            lim_izq = false;


            if (piece.y >= 0) {
                for (int j = 0; j < piece.h; j++) {
                    if (fields[piece.x - 1][piece.y + j] != 0) {

                        lim_izq = true;

                    }

                }
            }


            if (!lim_izq) {
                if (piece.x != 0) {
                    piece.x--;
                }
            }
        }
    }

    public void render(float deltaTime) {


        Graphics g = game.getGraphics();
        for (int i = 0; i <WORLD_WIDTH; i ++) {

            for (int j = 0; j < WORLD_HEIGHT; j++) {

                if(fields[i][j] != 0)
                {
                    if(fields[i][j] == 1)
                    {
                        g.drawPixmap(Assets.tile_azul_flojo, i*32, j*32);
                    }
                    if(fields[i][j] == 2)
                    {
                        g.drawPixmap(Assets.tile_amarillo, i*32, j*32);
                    }
                    if(fields[i][j] == 3)
                    {
                        g.drawPixmap(Assets.tile_lila, i*32, j*32);
                    }
                    if(fields[i][j] == 4)
                    {
                        g.drawPixmap(Assets.tile_verde, i*32, j*32);
                    }
                    if(fields[i][j] == 5)
                    {
                        g.drawPixmap(Assets.tile, i*32, j*32);
                    }
                    if(fields[i][j] == 6)
                    {
                        g.drawPixmap(Assets.tile_azul, i*32, j*32);
                    }


                }

            }

        }

        for (int i = 0; i < piece.w; i++) {

            for (int j = 0; j < piece.h; j++) {
                if(piece.forma[i][j] != 0) {


                    g.drawPixmap(piece.Tile, (piece.x + i) * 32, (piece.y + j) * 32);


                }
            }
        }


    }








}