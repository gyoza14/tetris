package cat.epiabalmes.damviod.tetris.Tetris;

/**
 * Created by sergi on 10/01/2016.
 */

import cat.epiabalmes.damviod.tetris.framework.Pixmap;

class Pieza{
    int current = 1;
    int x;
    int y;
    int w;
    int h;
    int [][] forma;
    public Pixmap Tile;


    public Pieza(int current) {

        //Creador de las Piezas
        this.current = current;
        x = 5;
        y = -2;

        switch (current) {
            case 1:
                y = -4;

                w = 1;
                h = 4;
            /*   Se crea la siguiente figura:
             *
             *       [1]
             *       [1]
             *       [1]
             *       [1]     */
                forma = new int[w][h];
                forma[0][0] = current;
                forma[0][1] = current;
                forma[0][2] = current;
                forma[0][3] = current;
                Tile = Assets.tile_azul_flojo;
                break;


            case 2:

            w = 2;
            h = 2;

             /*   Se crea la siguiente figura:
             *
             *
             *
             *       [1][1]
             *       [1][1]     */
            forma = new int[w][h];
            forma[0][0] = current;
            forma[0][1] = current;
            forma[1][0] = current;
            forma[1][1] = current;
            Tile = Assets.tile_amarillo;
            break;

            case 3:

            w = 3;
            h = 2;
            x--;
             /*   Se crea la siguiente figura:
             *
             *
             *
             *       [1]
             *    [1][1][1]     */
            forma = new int[w][h];
            forma[1][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            forma[2][1] = current;
            Tile = Assets.tile_lila;
                break;

            case 4:
            w = 3;
            h = 2;

            y = -1;
            x = 2;

             /*   Se crea la siguiente figura:
             *
             *
             *
             *       [1][1]
             *    [1][1]     */
            forma = new int[w][h];
            forma[1][0] = current;
            forma[2][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            Tile = Assets.tile_verde;
                break;

            case 5:
            w = 3;
            h = 2;

            x--;
             /*   Se crea la siguiente figura:
             *
             *
             *
             *    [1][1]
             *       [1][1]     */
            forma = new int[w][h];
            forma[0][0] = current;
            forma[1][0] = current;
            forma[2][1] = current;
            forma[1][1] = current;
            Tile = Assets.tile;
                break;

            case 6:
            w = 3;
            h = 2;

            x--;
             /*   Se crea la siguiente figura:
             *
             *
             *
             *     [1]
             *     [1][1][1]     */
            forma = new int[w][h];
            forma[0][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            forma[2][1] = current;
            Tile = Assets.tile_azul;
                break;

            default:
                break;

        }


    }


}